<?php


function LibFontAwesome_upgrade($version_base, $version_ini)
{
    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();

    $addon = bab_getAddonInfosInstance('LibFontAwesome');

    if (false === $functionalities->register('FontAwesome', $addon->getPhpPath() . 'FontAwesome.php')) {
        return false;
    }

    return true;
}



function LibFontAwesome_onDeleteAddon() {

    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();

    if (false === $functionalities->unregister('FontAwesome')) {
        return false;
    }

    return true;
}