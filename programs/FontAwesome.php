<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2008 by CANTICO ({@link http://www.cantico.fr})
 */
require_once 'base.php';


/**
 * The version of jquery
 */
define('FONTAWESOME_VERSION', '4_6_3');


/**
 * The JQUERY_COMPRESSION_xxx constants define the different types of compression
 * available for jquery files.
 *
 * @deprecated	since 1.3.2. Use Func_Jquery constants instead
 */
define('FONTAWESOME_COMPRESSION_MINIFIED', '.min');
define('FONTAWESOME_COMPRESSION_NONE', '');


/**
 * This functionality provides the possibility to includes jquery scripts in a page.
 */
class Func_FontAwesome extends bab_Functionality
{
    private $baseStylesPath;
    private $compressionType;

    /**
     * The page object in which scripts will be included.
     * @var object
     */
    private $page;

    /**
     * @var string	Included files will be compressed (recommended for production).
     */
    const COMPRESSION_MINIFIED = '.min';

    /**
     * @var string	Included files will not be compressed (recommended for javascript debugging).
     */
    const COMPRESSION_NONE = '';


    public $deferred = false;

    function __construct()
    {
        $addon = bab_getAddonInfosInstance('LibFontAwesome');

        $this->baseStylesPath = $addon->getStylePath();

        if (isset($_COOKIE['bab_debug'])) {
            $this->setCompression(self::COMPRESSION_NONE);
        } else {
            $this->setCompression(self::COMPRESSION_MINIFIED);
        }
        $this->page = bab_getInstance('babBody');
    }


    /**
     * Selects the compression that will be used for included javascript files.
     *
     * The $compressionType string can be any of the following constants:
     * - FONTAWESOME_COMPRESSION_MINIFIED
     * - FONTAWESOME_COMPRESSION_NONE
     *
     * @param string $compressionType		One of the FONTAWESOME_COMPRESSION_xxx constants.
     * @return string		The actually selected compression type.
     */
    function setCompression($compressionType)
    {
        switch ($compressionType) {
            case self::COMPRESSION_MINIFIED:
            case self::COMPRESSION_NONE:
                $this->compressionType = $compressionType;
                break;
        }
        return $this->compressionType;
    }


    function getDescription() {
        return 'Includes Font Awesome css';
    }


    protected function useCDN()
    {
        return (!isset($GLOBALS['babUseFontAwesomeCDN']) || (isset($GLOBALS['babUseFontAwesomeCDN']) && $GLOBALS['babUseFontAwesomeCDN'] == true));
    }

    /**
     * Returns the url to the specified javascript file.
     *
     * @param string $filename
     * @return string
     */
    function getUrl()
    {
        if ($this->useCDN()) {
            return '//use.fontawesome.com/bd49dba6eb.js';
        } else {
            switch ($this->compressionType) {
                case self::COMPRESSION_MINIFIED:
                    return $this->baseStylesPath . 'css/font-awesome.css';

                case self::COMPRESSION_NONE:
                default:
                    return $this->baseStylesPath . 'css/font-awesome.css';
            }
        }
        return '';
    }

    /**
     * Adds the specified jquery javascript file to the current ovidentia page.
     *
     * @param string 	$filename
     * @param bool		$deferred
     * @access protected
     */
    function includeCore()
    {
        include_once $GLOBALS['babInstallPath'] . '/utilit/devtools.php';

        if ($this->useCDN()) {
            $this->page->addJavascriptFile($this->getUrl());
        } else {
            $this->page->addStyleSheet($this->getUrl());
        }
    }
}
