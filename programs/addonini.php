;<?php/*

[general]
name="LibFontAwesome"
version="4.6.3"
description="Font Awesome (icon library) package for Ovidentia"
description.fr="Font Awesome (Bibliothèque d'icone) pour Ovidentia"
encoding="UTF-8"
delete=1
ov_version="7.0.0"
php_version="5.1"
mysql_character_set_database="latin1,utf8"
addon_access_control="0"
author="Cantico (support@cantico.fr)"
icon="icon.png"

;*/?>
